const productList = document.getElementById("product-list");

fetch('https://dummyjson.com/products?limit=23&select=images,title,price,description,stock,brand')
  .then(res => res.json())
  .then(data => {
    console.log(data);
    const productsWithThumbnail = data.products.filter(product => {
      return product.images.some(image => image.endsWith("thumbnail.jpg"));
    });

    productsWithThumbnail.forEach(product => {
      const thumbnailImage = product.images.find(image => image.endsWith("thumbnail.jpg"));

      const productItem = document.createElement('div');
      productItem.classList.add('col-lg-3', 'col-md-4', 'col-sm-6', 'mb-4');
      productItem.innerHTML = `
        <div class="card h-100">
          <img src="${thumbnailImage}" class="card-img-top thumbnail-img" alt="${product.title}">
          <div class="card-body">
            <h3 class="h3 mb-3">${product.title}</h3>
            <p class="card-text mb-0">Harga: ${product.price}K</p>
            <p class="card-text mb-0">Deskripsi: ${product.description}</p>
            <p class="card-text mb-0">Stok: ${product.stock}</p>
            <p class="card-text mb-0">Merek: ${product.brand}</p>
          </div>
        </div>
      `;

      if (productList.children.length % 4 === 0) {
        const row = document.createElement('div');
        row.classList.add('row');
        productList.appendChild(row);
      }

      const lastRow = productList.lastElementChild;
      lastRow.appendChild(productItem);
    });
  })
  .catch(error => {
    console.error('Error fetching data:', error);
  });
