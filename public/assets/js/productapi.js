const productList = document.getElementById("product-list");

fetch('https://dummyjson.com/products?limit=12&select=title,price,description,stock,brand')
  .then(res => res.json())
  .then(data => {
    console.log(data);
    data.products.forEach(product => {
      const productItem = document.createElement('div');
      productItem.innerHTML = `
          <h2>${product.title}</h2>
          <p>Harga: ${product.price}</p>
          <p>Deskripsi: ${product.description}</p>
          <p>Stok: ${product.stock}</p>
          <p>Merek: ${product.brand}</p>
        `;
      productList.appendChild(productItem);
    });
  })
  .catch(error => {
    console.error('Error fetching data:', error);
  });

